module gitlab.com/AlastorTh/sportsrpc

go 1.14

require (
	github.com/alvaroloes/enumer v1.1.2
	github.com/cenkalti/backoff/v4 v4.0.2
	github.com/gorilla/mux v1.7.4
	github.com/spf13/viper v1.7.1
	go.uber.org/zap v1.15.0
)
