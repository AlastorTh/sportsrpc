ARG GOLANG_VERSION=latest
ARG DEBIAN_VERSION=buster-slim

FROM golang:${GOLANG_VERSION} as builder

WORKDIR /src

COPY . .

RUN mkdir -p /app && \
    go build -o /app ./cmd/line-provider

COPY ./cmd/line-provider/config.yaml /app

FROM debian:${DEBIAN_VERSION}

COPY --from=builder /app /app

WORKDIR /app

CMD ["/app/line-provider"]