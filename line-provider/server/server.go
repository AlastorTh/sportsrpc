package server

import (
	"context"
	"encoding/json"
	"errors"
	"net"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"sync"
	"syscall"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/AlastorTh/sportsrpc/line-core/models"
	"go.uber.org/zap"
)

type Config struct {
	Port     int
	Interval time.Duration
}

type Server struct {
	httpServer *http.Server
	logger     *zap.Logger

	lines map[models.SportType]models.CoefficientType
	mutex *sync.RWMutex
}

func New(log *zap.Logger, config *Config) *Server {
	log = log.Named("Line Provider")

	s := &Server{
		logger: log,

		lines: make(map[models.SportType]models.CoefficientType),
		mutex: &sync.RWMutex{},
	}

	s.initHttp(config)

	return s
}

func (s *Server) Push(lines ...models.Line) {
	s.mutex.Lock()
	defer s.mutex.Unlock()
	for _, line := range lines {
		s.lines[line.Sport] = line.Coefficient
	}
}

func (s *Server) handlerSports(writer http.ResponseWriter, request *http.Request) {
	vars := mux.Vars(request)
	sport, ok := vars["sport"]
	if !ok {
		writer.WriteHeader(http.StatusBadRequest)
		return
	}

	s.mutex.RLock()
	defer s.mutex.RUnlock()
	coefficient, ok := s.lines[models.SportType(sport)]
	if !ok {
		writer.WriteHeader(http.StatusNotFound)
		return
	}

	lineMarshalled, err := json.Marshal(
		models.Line{
			Sport:       models.SportType(sport),
			Coefficient: models.CoefficientType(coefficient),
		},
	)
	if err != nil {
		s.logger.Error("Can't marshal", zap.Error(err))
		writer.WriteHeader(http.StatusInternalServerError)
		return
	}
	if _, err := writer.Write(lineMarshalled); err != nil {
		s.logger.Error("Can't write response", zap.Error(err))
		writer.WriteHeader(http.StatusInternalServerError)
	}
}

func (s *Server) initHttp(config *Config) {
	router := mux.NewRouter()
	router.Use(loggingMiddleware(s.logger))

	api := router.PathPrefix(`/api/v1`).Subrouter()
	api.Path(`/lines/{sport}`).Methods("GET").HandlerFunc(s.handlerSports)

	s.httpServer = &http.Server{
		Addr:    net.JoinHostPort("", strconv.Itoa(config.Port)),
		Handler: router,
	}
}

func (s *Server) shutdown(ctx context.Context) error {
	defer s.logger.Info("Shutdown")
	return s.httpServer.Shutdown(ctx)
}

func loggingMiddleware(log *zap.Logger) mux.MiddlewareFunc {
	return func(handler http.Handler) http.Handler {
		return http.HandlerFunc(
			func(writer http.ResponseWriter, request *http.Request) {
				log.Info(
					request.Method,
					zap.String("url", request.URL.String()),
					zap.Any("query", request.URL.Query()),
				)
				handler.ServeHTTP(writer, request)
			},
		)
	}
}

func (s *Server) ListenAndServe() error {
	go func() {
		if err := s.httpServer.ListenAndServe(); !errors.Is(err, http.ErrServerClosed) {
			s.logger.Error("Serve HTTP", zap.Error(err))
		}
	}()
	s.logger.Info("Listening HTTP", zap.String("Addr", s.httpServer.Addr))

	s.logger.Info("Service started")
	return s.wait()
}

func (s *Server) wait() error {
	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)

	<-signalChan
	return s.shutdown(context.Background())
}
