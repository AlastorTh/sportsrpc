package models

type SportType string
type CoefficientType float64

type Line struct {
	Sport       SportType
	Coefficient CoefficientType
}
