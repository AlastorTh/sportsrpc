package cmd

import (
	"flag"
	"fmt"

	"github.com/spf13/viper"
	"gitlab.com/AlastorTh/sportsrpc/line-processor/server"
)

func ParseFlagsAndConfig() (*server.Config, error) {
	configFlag := flag.String("config", "config.yaml", "config path")
	flag.Parse()

	configViper := viper.New()
	configViper.SetConfigFile(*configFlag)

	if err := configViper.ReadInConfig(); err != nil {
		return nil, fmt.Errorf("can't read config file: %w", err)
	}

	config := &server.Config{}
	if err := configViper.Unmarshal(config); err != nil {
		return nil, fmt.Errorf("can't parse config file: %w", err)
	}

	return config, nil
}
