package sports

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"path"

	"github.com/cenkalti/backoff/v4"
	"gitlab.com/AlastorTh/sportsrpc/line-core/models"
	"go.uber.org/zap"
)

type Storage struct {
	config StorageConfig
	logger *zap.Logger

	linesBuffer []*models.Line
}

type StorageConfig struct {
	ProviderAddr string
	Timeouts     Timeouts
}

func NewStorage(
	logger *zap.Logger,
	config StorageConfig,
) *Storage {
	s := &Storage{
		config: config,
		logger: logger.Named("Storage"),
	}
	return s
}

func (s *Storage) fetchSport(sport Sport) (*models.Line, error) {
	url := url.URL{
		Scheme: "http",
		Host:   s.config.ProviderAddr,
		Path: path.Join(
			"api",
			"v1",
			"lines",
			sport.String(),
		),
	}
	resp, err := http.Get(url.String())
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	raw, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	line := &models.Line{}
	if err := json.Unmarshal(raw, line); err != nil {
		return nil, err
	}

	s.logger.Info("Fetched sport", zap.String("Sport", sport.String()))
	return line, nil
}

func (s *Storage) fetchAllSports() error {
	var lines []*models.Line

	for _, sport := range Sports {
		line, err := s.fetchSport(sport)
		if err != nil {
			return err
		}
		lines = append(lines, line)
	}

	s.linesBuffer = lines
	s.logger.Info("Sport lines fetched")
	return nil
}

func (s *Storage) Start(ctx context.Context) error {
	if err := backoff.Retry(
		s.fetchAllSports,
		backoff.WithContext(backoff.NewExponentialBackOff(), ctx),
	); err != nil {
		return fmt.Errorf("can't fetch sport lines: %w", err)
	}

	s.config.Timeouts.Start(func(sport Sport) {
		_, err := s.fetchSport(sport)
		if err != nil {
			s.logger.Warn("Can't fetch sport", zap.String("sport", sport.String()), zap.Error(err))
		}
	})

	s.logger.Info("Started")
	return nil
}

func (s *Storage) Shutdown() error {
	defer s.logger.Info("Shutdown")
	s.config.Timeouts.Stop()
	return nil
}
