package sports

//go:generate go run github.com/alvaroloes/enumer -type=Sport -transform=snake
type Sport int

var Sports = []Sport{Baseball, Football, Soccer}

const (
	Baseball Sport = iota
	Football
	Soccer
)
