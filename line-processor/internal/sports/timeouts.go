package sports

import "time"

type Timeouts struct {
	Baseball time.Duration
	Football time.Duration
	Soccer   time.Duration

	exit chan struct{}
}

func (t *Timeouts) Start(handler func(Sport)) {
	t.exit = make(chan struct{}, 1)

	baseballTicker := time.NewTicker(t.Baseball)
	defer baseballTicker.Stop()

	footballTicker := time.NewTicker(t.Football)
	defer footballTicker.Stop()

	soccerTicker := time.NewTicker(t.Soccer)
	defer soccerTicker.Stop()

	go func() {
		for {
			select {
			case <-t.exit:
				return
			case <-baseballTicker.C:
				go handler(Baseball)
			case <-footballTicker.C:
				go handler(Football)
			case <-soccerTicker.C:
				go handler(Soccer)
			}
		}
	}()
}

func (t *Timeouts) Stop() {
	close(t.exit)
}
