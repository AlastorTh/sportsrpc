package health

import (
	"context"
	"errors"
	"net"
	"net/http"
	"strconv"
	"sync"

	"github.com/gorilla/mux"
	"go.uber.org/zap"
)

type Server struct {
	httpServer *http.Server
	logger     *zap.Logger

	health Health
	mutex  *sync.RWMutex
}

type Config struct {
	Port int
}

type Health struct {
	Ready bool
}

func New(log *zap.Logger, config Config) *Server {
	log = log.Named("HealtAPI")

	s := &Server{
		logger: log,

		mutex: &sync.RWMutex{},
	}

	s.initHttp(config)

	return s
}

func (s *Server) initHttp(config Config) {
	router := mux.NewRouter()
	router.Use(loggingMiddleware(s.logger))

	router.Path("/ready").Methods("GET").HandlerFunc(
		func(writer http.ResponseWriter, request *http.Request) {
			health := s.get()
			if health.Ready {
				writer.WriteHeader(http.StatusOK)
				return
			}
			writer.WriteHeader(http.StatusInternalServerError)
		},
	)

	s.httpServer = &http.Server{
		Addr:    net.JoinHostPort("", strconv.Itoa(config.Port)),
		Handler: router,
	}
}

func (s *Server) Setup(health Health) {
	s.mutex.Lock()
	defer s.mutex.Unlock()
	s.health = health
}

func (s *Server) get() Health {
	s.mutex.RLock()
	defer s.mutex.RUnlock()
	return s.health
}

func (s *Server) Start() {
	defer s.logger.Info("Listening HTTP", zap.String("Addr", s.httpServer.Addr))
	go func() {
		if err := s.httpServer.ListenAndServe(); !errors.Is(err, http.ErrServerClosed) {
			s.logger.Error("Serve HTTP", zap.Error(err))
		}
	}()
}

func (s *Server) Shutdown(ctx context.Context) error {
	defer s.logger.Info("Shutdown")
	return s.httpServer.Shutdown(ctx)
}

func loggingMiddleware(log *zap.Logger) mux.MiddlewareFunc {
	return func(handler http.Handler) http.Handler {
		return http.HandlerFunc(
			func(writer http.ResponseWriter, request *http.Request) {
				log.Info(
					request.Method,
					zap.String("url", request.URL.String()),
					zap.Any("query", request.URL.Query()),
				)
				handler.ServeHTTP(writer, request)
			},
		)
	}
}
