package server

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gitlab.com/AlastorTh/sportsrpc/line-processor/internal/health"
	"gitlab.com/AlastorTh/sportsrpc/line-processor/internal/sports"
	"go.uber.org/zap"
)

type Config struct {
	HTTP health.Config
	GRPC struct {
		Port string
	}
	Storage        sports.StorageConfig
	StartupTimeout time.Duration
}

func New(
	logger *zap.Logger,
	config *Config,
) *Server {
	logger = logger.Named("Line Processor")

	return &Server{
		logger: logger,

		storage:        sports.NewStorage(logger, config.Storage),
		healtServer:    health.New(logger, config.HTTP),
		startupTimeout: config.StartupTimeout,
	}
}

type Server struct {
	logger         *zap.Logger
	startupTimeout time.Duration

	storage     *sports.Storage
	healtServer *health.Server
}

func (s *Server) ListenAndServe() error {
	s.healtServer.Start()

	ctx, cancel := context.WithTimeout(context.Background(), s.startupTimeout)
	defer cancel()
	if err := s.storage.Start(ctx); err != nil {
		return fmt.Errorf("can't start storage service: %w", err)
	}

	s.logger.Info("Service started")
	return s.wait()
}

func (s *Server) wait() error {
	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)

	<-signalChan
	return s.shutdown(context.Background())
}

func (s *Server) shutdown(ctx context.Context) error {
	var errReturned error
	if err := s.healtServer.Shutdown(ctx); err != nil {
		s.logger.Error("Shutdown", zap.Error(err))
		errReturned = err
	}
	if err := s.storage.Shutdown(); err != nil {
		s.logger.Error("Shutdown", zap.Error(err))
		errReturned = err
	}

	s.logger.Info("Service shutdown")
	return errReturned
}
