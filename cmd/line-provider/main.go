package main

import (
	"fmt"
	"math/rand"
	"os"
	"time"

	"gitlab.com/AlastorTh/sportsrpc/line-core/models"
	"gitlab.com/AlastorTh/sportsrpc/line-provider/cmd"
	"gitlab.com/AlastorTh/sportsrpc/line-provider/server"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

func main() {
	config, err := cmd.ParseFlagsAndConfig()
	if err != nil {
		errorf("%s\n", err)
	}

	logger, err := newLogger()
	if err != nil {
		errorf("Can't initialize logger: %s\n", err)
	}
	server := server.New(logger, config)

	generatorExit := linesGenerate(logger, server, config.Interval)
	if err := server.ListenAndServe(); err != nil {
		errorf("%s\n", err)
	}
	close(generatorExit)
}

func newLogger() (*zap.Logger, error) {
	config := zap.NewDevelopmentConfig()
	config.EncoderConfig.EncodeLevel = zapcore.CapitalColorLevelEncoder
	return config.Build()
}

func errorf(format string, args ...interface{}) {
	fmt.Fprintf(os.Stderr, format, args...)
	os.Exit(1)
}

func linesGenerate(logger *zap.Logger, srv *server.Server, interval time.Duration) chan<- struct{} {
	exit := make(chan struct{})
	go func() {
		ticker := time.NewTicker(interval)
		defer ticker.Stop()
		for {
			select {
			case <-exit:
				return
			case <-ticker.C:
				srv.Push(randomLines()...)
				logger.Info("Pushed new lines")
			}
		}
	}()

	return exit
}

func randomLines() []models.Line {
	lines := []models.Line{
		{
			Sport: "baseball",
		},
		{
			Sport: "football",
		},
		{
			Sport: "soccer",
		},
	}

	for idx := range lines {
		lines[idx].Coefficient = models.CoefficientType(rand.Float64())
	}
	return lines
}
