package main

import (
	"fmt"
	"os"

	"gitlab.com/AlastorTh/sportsrpc/line-processor/cmd"
	"gitlab.com/AlastorTh/sportsrpc/line-processor/server"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

func main() {
	config, err := cmd.ParseFlagsAndConfig()
	if err != nil {
		errorf("%s\n", err)
	}

	logger, err := newLogger()
	if err != nil {
		errorf("Can't initialize logger: %s\n", err)
	}
	server := server.New(logger, config)

	if err := server.ListenAndServe(); err != nil {
		errorf("%s\n", err)
	}
}

func newLogger() (*zap.Logger, error) {
	config := zap.NewDevelopmentConfig()
	config.EncoderConfig.EncodeLevel = zapcore.CapitalColorLevelEncoder
	return config.Build()
}

func errorf(format string, args ...interface{}) {
	fmt.Fprintf(os.Stderr, format, args...)
	os.Exit(1)
}
