CI_IMAGE_NAME := "ci-builder"
CI_IMAGE_VERSION := "0.0.1"

.PHONY: build_ci
build_ci:
	sudo docker build -t ${CI_IMAGE_NAME}:${CI_IMAGE_VERSION} ./ci

.PHONY: build
build:
	go mod vendor
	docker-compose build

.PHONY: run
run:
	docker-compose up

.PHONY: test
test:
	go test ./...

.PHONY: lint
lint:
	golangci-lint run